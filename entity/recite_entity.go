package entity

import "gorm.io/gorm"

const (
	ReciteTableName = "recite"
)

// ArticleModel is a model for entity.Article
type Recite struct {
	gorm.Model
	Id       Komik     `gorm:"foreignKey:Id" json:"id"`
	Platform string `gorm:"type:varchar;not_null" json:"platform"`
}

func NewRecite(id Komik, platform string) *Recite {
	return &Recite{
		Id:       id,
		Platform: platform,
	}
}

// TableName specifies table name for ArticleModel.
func (model *Recite) TableName() string {
	return ReciteTableName
}
