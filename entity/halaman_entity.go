package entity

import "gorm.io/gorm"

const (
	HalamanTableName = "halaman"
)

// ArticleModel is a model for entity.Article
type Halaman struct {
	gorm.Model
	Id       Detailed `gorm:"foreignKey:Id" json:"id"`
	Chapter  int      `gorm:"type:int;not_null" json:"chapter"`
	Volume 	 int      `gorm:"type:int;not_null" json:"volume"`
}

func NewHalaman(id Detailed, chapter, volume int) *Halaman {
	return &Halaman{
		Id:       id,
		Chapter:  chapter,
		Volume:   volume,
	}
}

// TableName specifies table name for ArticleModel.
func (model *Halaman) TableName() string {
	return HalamanTableName
}

