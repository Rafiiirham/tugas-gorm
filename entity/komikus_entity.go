package entity

import "gorm.io/gorm"

const (
	KomikusTableName = "komikus"
)

// ArticleModel is a model for entity.Article
type Komikus struct {
	gorm.Model
	Id   Komik     `gorm:"foreignKey:Id" json:"id"`
	Name string 	`gorm:"type:varchar;not_null" json:"name"`
}

func NewKomikus(id Komik, name string) *Komikus {
	return &Komikus{
		Id:   id,
		Name: name,
	}
}

// TableName specifies table name for ArticleModel.
func (model *Komikus) TableName() string {
	return ActorTableName
}