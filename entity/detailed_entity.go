package entity

import "gorm.io/gorm"

const (
	DetailedTableName = "detailed"
)

// ArticleModel is a model for entity.Article
type Detailed struct {
	gorm.Model
	Id       Komik   `gorm:"foreignKey:Id" json:"id"`
	Chapter  int      `gorm:"type:int;not_null" json:"chapter"`
	Volume 	 int      `gorm:"type:int;not_null" json:"volume"`
	Year     int 	  `gorm:"type:int;not_null" json:"year"`
}

func NewDetailed(id Komik, chapter, volume, year int) *Detailed {
	return &Detailed{
		Id:       id,
		Chapter:  chapter,
		Volume:   volume,
		Year:     year,
	}
}

// TableName specifies table name for ArticleModel.
func (model *Detailed) TableName() string {
	return DetailedTableName
}
