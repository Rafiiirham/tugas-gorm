package entity

import (
	"github.com/gofrs/uuid"
	"gorm.io/gorm"
)

const (
	KomikTableName = "komikchapter_info"
)

// ArticleModel is a model for entity.Article
type Komik struct {
	gorm.Model
	Id       uuid.UUID `gorm:"type:uuid;primary_key" json:"id"`
	Title    string    `gorm:"type:varchar;not_null" json:"title"`
	Novelis  string     `gorm:"type:varchar;null" json:"novelis"`
}

func NewKomik(id uuid.UUID, title, novelis string) *Komik {
	return &Komik{
		Id:       id,
		Title:    title,
		Novelis:  novelis,
	}
}

// TableName specifies table name for ArticleModel.
func (model *Komik) TableName() string {
	return KomikTableName
}
